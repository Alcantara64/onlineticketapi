<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Ticket extends REST_Controller {

    public function __construct() {
        parent::__construct();

		//load ticket model
        $this->load->model('tickets');
    }

	public function ticket_get($id = 0) {
		//returns all rows if the id parameter doesn't exist,
		//otherwise single row will be returned
		$tickets = $this->tickets->getRows($id,'tickets');

		//check if the ticket data exists
		if(!empty($tickets)){
			//set the response and exit
			//OK (200) being the HTTP response code
			$this->response($tickets, REST_Controller::HTTP_OK);
		}else{
			//set the response and exit
			//NOT_FOUND (404) being the HTTP response code
			$this->response([
				'status' => FALSE,
				'message' => 'No ticket were found.'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}
  public function tickettype_get($id = 0) {
    //returns all rows if the id parameter doesn't exist,
    //otherwise single row will be returned
    $tickets = $this->tickets->getRows($id,'tickettype');

    //check if the ticket data exists
    if(!empty($tickets)){
      //set the response and exit
      //OK (200) being the HTTP response code
      $this->response($tickets, REST_Controller::HTTP_OK);
    }else{
      //set the response and exit
      //NOT_FOUND (404) being the HTTP response code
      $this->response([
        'status' => FALSE,
        'message' => 'No ticket were found.'
      ], REST_Controller::HTTP_NOT_FOUND);
    }
  }

	public function ticket_post() {
		$userData = array();
		$userData['name'] = $this->post('name');
		$userData['email'] = $this->post('email');
		$userData['status'] = 1;
    $userData['phone'] = $this->post('phone');
    $userData['tickettype_id'] = $this->post('type_id');
		if(!empty($userData['name']) && !empty($userData['phone'])  && !empty($userData['tickettype_id']) && !empty($userData['email']) && !empty($userData['phone'])){
			//insert user data
			$insert = $this->tickets->insert($userData,'ticket');

			//check if the ticket data inserted
			if($insert){
				//set the response and exit
				$this->response([
					'status' => TRUE,
					'message' => 'ticket has been added successfully.'
				], REST_Controller::HTTP_OK);
			}else{
				//set the response and exit
				$this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
			}
        }else{
			//set the response and exit
			//BAD_REQUEST (400) being the HTTP response code
            $this->response("Provide complete ticket information to create.", REST_Controller::HTTP_BAD_REQUEST);
		}
	}

  public function Tickettype_post() {
    $userData = array();
    $userData['name'] = $this->post('name');
    $userData['amount'] = $this->post('amount');



    if(!empty($userData['name']) && !empty($userData['amount']) ){
      //insert user data
      $insert = $this->tickets->insert($userData,'tickettype');

      //check if the ticket data inserted
      if($insert){
        //set the response and exit
        $this->response([
          'status' => TRUE,
          'message' => 'ticket type has been added successfully.'
        ], REST_Controller::HTTP_OK);
      }else{
        //set the response and exit
        $this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
      }
        }else{
      //set the response and exit
      //BAD_REQUEST (400) being the HTTP response code
            $this->response("Provide complete ticket information to create.", REST_Controller::HTTP_BAD_REQUEST);
    }
  }

	public function ticket_put() {
		$userData = array();
		$id = $this->put('id');
    $userData['name'] = $this->post('name');
    $userData['email'] = $this->post('email');
    $userData['status'] = 1;
    $userData['phone'] = $this->post('phone');
    $userData['tickettype_id'] = $this->post('type_id');

		if(!empty($id)){
			//update user data
			$update = $this->tickets->update($userData, $id,'ticket');

			//check if the user data updated
			if($update){
				//set the response and exit
				$this->response([
					'status' => TRUE,
					'message' => 'ticket has been updated successfully.'
				], REST_Controller::HTTP_OK);
			}else{
				//set the response and exit
				$this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
			}
        }else{
			//set the response and exit
            $this->response("Provide complete ticket information to update.", REST_Controller::HTTP_BAD_REQUEST);
		}
	}
// update the ticket type
  public function tickettype_put() {
		$userData = array();
		$id = $this->put('id');
    $userData['name'] = $this->post('name');
    $userData['amount'] = $this->post('amount');


		if(!empty($id)){
			//update user data
			$update = $this->tickets->update($userData, $id,'tickettype');

			//check if the user data updated
			if($update){
				//set the response and exit
				$this->response([
					'status' => TRUE,
					'message' => 'ticket type has been updated successfully.'
				], REST_Controller::HTTP_OK);
			}else{
				//set the response and exit
				$this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
			}
        }else{
			//set the response and exit
            $this->response("Provide complete ticket information to update.", REST_Controller::HTTP_BAD_REQUEST);
		}
	}

	public function ticket_delete($id){
        //check whether post id is not empty
        if($id){
            //delete post
            $delete = $this->tickets->delete($id);

            if($delete){
                //set the response and exit
				$this->response([
					'status' => TRUE,
					'message' => 'ticket has been removed successfully.'
				], REST_Controller::HTTP_OK);
            }else{
                //set the response and exit
				$this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
            }
        }else{
			//set the response and exit
			$this->response([
				'status' => FALSE,
				'message' => 'No ticket were found.'
			], REST_Controller::HTTP_NOT_FOUND);
		}
    }
}

?>
