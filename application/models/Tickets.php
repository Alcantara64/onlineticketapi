<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tickets extends CI_Model {

    public function __construct() {
        parent::__construct();

		//load database library
        $this->load->database();
    }

    /*
     * Fetch ticket data
     */
    function getRows($id = "",$table){
        if(!empty($id)){
            $query = $this->db->get_where($table, array('id' => $id));
            return $query->row_array();
        }else{
            $query = $this->db->get($table);
            return $query->result_array();
        }
    }

    /*
     * Insert ticket data
     */
    public function insert($data = array(),$table) {
		if(!array_key_exists('created_at', $data)){
			$data['created_at'] = date("Y-m-d H:i:s");
		}
		if(!array_key_exists('updated_at', $data)){
			$data['updated_at'] = date("Y-m-d H:i:s");
		}
        $insert = $this->db->insert($table, $data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    /*
     * Update ticket and ticket type data
     */
    public function update($data, $id, $table) {
        if(!empty($data) && !empty($id)){
			if(!array_key_exists('updated_at', $data)){
				$data['updated_at'] = date("Y-m-d H:i:s");
			}
            $update = $this->db->update($table, $data, array('id'=>$id));
            return $update?true:false;
        }else{
            return false;
        }
    }

    /*
     * Delete ticket data
     */
    public function delete($id){
        $delete = $this->db->delete('tickets',array('id'=>$id));
        return $delete?true:false;
    }

}
?>
